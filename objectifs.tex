\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{lmodern}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{titlesec}
\author{Franck \textsc{Charras}}
\title{\textsc{Wordnet et word2vec}}
\begin{document}
\maketitle

\section{Objectifs I}

\paragraph{} Nous nous intéressons à deux outils de traitement automatique du langage: d'une part WordNet, une base de données lexicale encodée à la main et regroupant les lemmes de la langue anglaise en ensembles de synonymes, ainsi que des relations entre les ensembles de synonymes: relations d'hypernymie et hyponymie, de méronymie et d'holonymie, définitions et exemples d'utilisation. D'autre part un modèle Word2Vec de représentation vectorielle des mots dans un espace de grande dimension, entrainé par apprentissage non supervisé avec une méthode de prédiction de contexte sur un corpus Google News de 100 milliards de mots. 

\paragraph{} Les interrogations portent sur les liens que peuvent entretenir les deux outils. Quelle part des relations encodées dans WordNet sont capturées par le modèle Word2Vec ? celui-ci a été entraîné sans but d'une tâche précise, et pourrait trouver des applications pour plusieurs tâches. Réciproquement, quelles relations spatiales des vecteurs pourraient prédire ou apporter des informations complémentaires aux données de WordNet ? Enfin, compte tenu des réponses aux questions précédentes, peut-on à partir d'une représentation type word2vec, construire à l'aide de wordnet de nouvelles représentations vectorielles, lissées ou bien plus appropriées à certaines tâches ?

\paragraph{} On découpe ces questions avec la série d'objectifs suivant.

\subsection{Objectif 1: aperçu statistique des représentations de wordnet dans word2vec}

\paragraph{} Un premier objectif est de mieux comprendre la distribution de word2vec à l'aide de statistiques de distances entre les objets de wordnet, par exemple: 
\begin{itemize}
\item distance moyenne entre des mots tirés au hasard
\item distance moyenne entre des synonymes
\item distance moyenne entre des antonymes
\item distance moyenne entre un lemme et son (ses) hypernymes
\item distance moyenne entre des co-hyponymes
\item distance moyenne entre la moyenne des représentations des co-hyponymes et leur hypernyme commun
\end{itemize}

Pour les synonymes ça n'améliore pas du tout...

\subsection{Objectif 2: prédiction d'hypernymes}

\paragraph{} Les relations d'hypernymie encodées dans wordnet forment un arbre dont les racines sont les mots les plus généraux (tels que \textit{entité} ou \textit{chose}) et les feuilles sont les mots qui ne possèdent pas d'hypernymes. On peut donc considérer une coupe dans cet arbre, c'est-à-dire un ensemble d'hypernymes non apparentés tels que toute feuille de l'arbre est hyponyme à un certain degré de l'un des hypernymes de cet ensemble. On peut par exemple construire cette coupe de manière à ce que les ensembles fils définis par les hypernymes aient une fréquence similaire fixée dans la langue anglaise.

\paragraph{} On peut alors penser à appliquer à la distribution donnée par word2vec une classification par clustering (par exemple par Knn) pour prédire le bon hypernyme de la coupe pour chacun des mots hyponymes.

\paragraph{} Cette approche peut se heurter au soucis d'identification des hypernymes puisqu'un mot peut posséder plusieurs hypernymes et donc les sous-arbres associés à chacun des hypernymes de la coupe peuvent être d'intersection non vide. Par exemple, les hypernymes associés à \textit{chien} sont \textit{canidé} et \textit{animal domestique}.

\subsection{Objectif 3: une meilleure connaissance des statistiques des hypernymes dans la langue anglaise}

Le modèle word2vec étant entrainé par la prédiction de mot selon son contexte, on saura mieux interpréter ses distributions si l'on s'intéresse à des statistiques telles que le pourcentage des phrases contenant un mot donné qui contiennent aussi son hypernyme (ou encore la part des phrases dans lesquels l'hypernyme et l'hyponyme sont utilisés dans des contextes similaires).

\subsection{Objectif 4: caractérisation de différents types de relations d'hypernymie}

\paragraph{} Bien que wordnet ne distingue qu'une seule relation d'hypernymie, on peut penser à utiliser les représentations vectorielles pour dégager plusieurs types de relations d'hypernymie, caractérisées par des transformations spécifiques de l'hyponyme vers l'hypernyme dans l'espace des vecteurs. Ces types de relations peuvent également s'obtenir par clustering (sur les différences des vecteurs).

\subsection{Objectif 5: mots, contextes et hypernymes}

\paragraph{} Une autre statistique à étudier est celle des relations dans word2vec entre les vecteurs des mots et les contextes, et en particulier ceux de leurs hypernymes, et de leurs contextes respectifs. En calculant par somme des représentations vectorielles des contextes des apparitions des mots dans un texte, on peut se demander quelles sont les distances moyennes entre un nom, son contexte, son hypernyme et le contexte de l'hypernyme dans l'espace des vecteurs. 

\paragraph{} En pratique cette statistique est difficile à obtenir puisque pour un mot ambigû plusieurs hypernymes sont possibles. On s'appuiera donc sur le corpus annoté des bons sens dans wordnet \textit{semcor} de 820411 mots pour cette tâche.

\paragraph{} Réciproquement, on peut par cette approche penser à améliorer les représentations vectorielles données par word2vec. En effet, pour un mot ambigu, on peut introduire les vecteurs correspondant à des combinaisons convexes du mot et des hypernymes associés à ses différents sens. Cette représentation sera meilleure si, pour un contexte donné, on peut prédire correctement le bon hypernyme (et donc le bon sens) du mot parmi les différents hypernymes possibles, ou au moins si elle améliore le classement de pertinence des différentes possibilités. On peut aussi l'évaluer sur sa capacité à prédire un mot étant donné son contexte, en considérant la position de la bonne réponse dans la liste des prédictions classée par probabilités décroissantes.

\subsection{Objectif 6: autre définition de l'hypernymie, et utilisation des définitions de wordnet}

\paragraph{} Au lieu de ne considérer que la relation d'hypernymie au sens de wordnet, on pourrait en envisager d'autres définitions, par exemple en les déduisant de certaines règles à partir d'un corpus, ou bien en exploitant les données des définitions ou des exemples que contient wordnet ?

\subsection{Objectif 7: transformation à noyau gaussien}

\paragraph{} Les additions ou soustractions sont susceptibles de perdre des données, par exemple si deux vecteurs s'annulent dans une somme. On peut penser à limiter cette perte en transformant les représentations vectorielles par une méthode à noyau gaussien.

\section{Objectifs II}

\subsection{Objectif 1: manipuler WordNet}

\paragraph{} Deux difficultés dans l'utilisation de WordNet: d'une part les définitions sont trop précises (chaque mot est associé à la fois à ses définitions concrètes et métaphoriques), d'autre part le graphe des relations d'hypernymie n'est pas exactement un arbre et il faut réfléchir à la bonne manière d'y définir une coupe. Deux approches pour répondre à ces difficultés:
\begin{itemize}
\item Faire des clusters sur les différents sens d'un mot (cf. plusieurs papiers sur le sujet)
\item Répartir des masses dans le graphe permettant de définir une coupe constituée des nœuds ayant une masse donnée. Il faut trouver la bonne définition de la "masse", qui doit en particulier prendre en compte que les sous-graphes associés aux fils d'un nœud peuvent être d'intersection non vide.
\end{itemize}

\subsection{Objectif 2: mieux connaître w2v et wordnet}

\paragraph{} Comment sont trouvés les bigrams ? (il semble qu'il y ait une filtration minimale des bigrams trop fréquents, par exemple il n'y a pas "is\_a" mais "is\_not", "is\_about", "is\_an"... 

\paragraph{} dans wordnet: 83118 unigrams, 54533 bigrams, 7766 trigrams, 1454 4-grams, 298 5-grams,...
\paragraph{} dans w2v: 929022 unigrams, 1341446 bigrams, 652241 trigrams, 77022 4-grams,...

\subsection{Objectif 3: clustering dans w2v}

\paragraph{} Faire des clusters en utilisant les algos de scikit-learn (K-means, clustering agglomeratif/hiérarchique). Mesurer leur qualité intrinsèque, ou par rapport aux coupes dans l'arbre des hypernymes. (indices de qualité disponible dans sklearn, distance de ward).

\paragraph{} Suite aux histogrammes produits, chercher les mots qui sont mal représentés dans w2v. Comprendre plus profondément les distributions des vecteurs en étudiant l'algorithme d'apprentissage pour expliquer des représentations pour des exemples concrets.

\paragraph{} Faire une liste exhaustive des partis pris de représentation que l'on peut trouver dans w2v et WordNet et qui pèsent nécessairement dans les mesures observées.

\paragraph{} Regarder les distances entre les cohyponymes présents dans le corpus Brown.

\end{document}